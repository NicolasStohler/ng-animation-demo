import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OneComponent } from './one/one.component';
import { TwoComponent } from './two/two.component';
import { ThreeComponent } from './three/three.component';
import { FiveComponent } from './five/five.component';
import { FourComponent } from './four/four.component';

const routes: Routes = [
  { path: '', redirectTo: 'one', pathMatch: 'full' },
  { path: 'one', component: OneComponent, data: { page: 'one' } },
  { path: 'two', component: TwoComponent, data: { page: 'two' } },
  { path: 'three', component: ThreeComponent, data: { page: 'three' } },
  { path: 'four', component: FourComponent, data: { page: 'four' } },
  { path: 'five', component: FiveComponent, data: { page: 'five' } },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
