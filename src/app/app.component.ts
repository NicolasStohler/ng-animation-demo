import { Component } from '@angular/core';
import { animate, animateChild, group, query, state, style, transition, trigger } from '@angular/animations';
import { routerTransition } from './router.animations';

// https://alligator.io/angular/animation-angular/

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    routerTransition,
    trigger('itemAnim', [
      transition(':enter', [
        style({transform: 'translateX(-100%)'}),
        animate(350)
      ]),
      transition(':leave', [
        group([
          animate('0.2s ease', style({
            transform: 'translate(150px,25px)'
          })),
          animate('0.5s 0.2s ease', style({
            opacity: 0
          }))
        ])
      ])
    ]),
    trigger('someCoolAnimation', [
      // https://blog.thoughtram.io/angular/2016/09/16/angular-2-animation-important-concepts.html
      state('fadeIn', style({opacity: 1, transform: 'scale(1.0)'})),
      state('fadeOut', style({opacity: 0, transform: 'scale(0.0)'})),
      transition('* => *', animate('0.8s'))
    ]),
    trigger('animRoutes', [
      // https://alligator.io/angular/animating-route-changes/
      transition('* <=> *', [
        group([
          query(
            ':enter',
            [
              style({
                opacity: 0,
                transform: 'translateY(9rem) rotate(-10deg)'
              }),
              animate(
                '0.8s cubic-bezier(0, 1.8, 1, 1.8)',
                style({ opacity: 1, transform: 'translateY(0) rotate(0)' })
              ),
              animateChild()
            ],
            { optional: true }
          ),
          query(
            ':leave',
            [animate('0.35s', style({ opacity: 0 })), animateChild()],
            { optional: true }
          )
        ])
      ])
    ])
  ]
})
export class AppComponent {
  animateIconState = '';
  items: Array<string> = new Array<string>();

  addItem(value: string): void {
    this.items.push(value);
  }

  removeItem(item: string) {
    const index = this.items.indexOf(item, 0);
    if (index > -1) {
      this.items.splice(index, 1);
    }
  }

  toggleAnimateIconState() {
    this.animateIconState = (this.animateIconState === 'fadeOut') ? 'fadeIn' : 'fadeOut';
  }

  getPage(outlet) {
    // https://alligator.io/angular/animating-route-changes/
    return outlet.activatedRouteData['page'] || 'one';
  }
}
