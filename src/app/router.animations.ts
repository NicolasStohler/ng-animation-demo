import {sequence, trigger, stagger, animate, style, group, query as q, transition, keyframes, animateChild} from '@angular/animations';
const query = (s, a, o= {optional: true}) => q(s, a, o);

export const routerTransition = trigger('routerTransition', [
  transition('* => *', [
    query(':enter, :leave', style({ position: 'fixed', width: '100%' })),
    // query(':enter', style({ transform: 'translateX(100%)' })),
    sequence([
      query(':leave', animateChild()),
      group([
        query(':leave', [
          style({ opacity: 1, transform: 'translateX(0%)' }),
          animate('300ms cubic-bezier(.65,.13,1,1)',
            style({ opacity: 0, transform: 'translateX(10%)' }))
        ]),
        query(':enter', [
          style({ opacity: 0, transform: 'translateX(10%)' }),
          animate('300ms 200ms cubic-bezier(.65,.13,1,1)',
            style({ opacity: 1, transform: 'translateX(0%)' }))
        ])
        // query(':leave', [
        //   style({ transform: 'translateX(0%)' }),
        //   animate('500ms cubic-bezier(.75,-0.48,.26,1.52)',
        //     style({ transform: 'translateX(-100%)' }))
        // ]),
        // query(':enter', [
        //   style({ transform: 'translateX(100%)' }),
        //   animate('500ms cubic-bezier(.75,-0.48,.26,1.52)',
        //     style({ transform: 'translateX(0%)' })),
        // ]),
      ]),
      query(':enter', animateChild()),
    ])
  ])
]);
